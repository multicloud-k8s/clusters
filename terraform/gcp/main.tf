provider "google" {
  version = "~> 2.18.0"
  region = "europe-north1"
  zone = "europe-north1-a"
  project = "multicloud-k8s-259914"
}

locals {
  network_name = "gke-network"
  subnetwork_name = "gke-subnet"
  ip_range_pods_name = "ip-range-pods"
  ip_range_services_name = "ip-range-scv"
}

data "google_client_config" "current" {
}

module "gcp-network" {
  source       = "terraform-google-modules/network/google"
  version      = "~> 1.4.0"
  project_id   = data.google_client_config.current.project
  network_name = local.network_name

  subnets = [
    {
      subnet_name   = local.subnetwork_name
      subnet_ip     = "10.0.0.0/17"
      subnet_region = data.google_client_config.current.region
    },
  ]

  secondary_ranges = {
    "${local.subnetwork_name}" = [
      {
        range_name    = local.ip_range_pods_name
        ip_cidr_range = "192.168.0.0/18"
      },
      {
        range_name    = local.ip_range_services_name
        ip_cidr_range = "192.168.64.0/18"
      },
    ]
  }
}

module "gke" {
  source                 = "terraform-google-modules/kubernetes-engine/google"
  project_id             = data.google_client_config.current.project
  name                   = "dev"
  regional               = false
  region                 = data.google_client_config.current.region
  zones                  = [data.google_client_config.current.zone]
  network                = module.gcp-network.network_name
  subnetwork             = module.gcp-network.subnets_names[0]
  ip_range_pods          = local.ip_range_pods_name
  ip_range_services      = local.ip_range_services_name
  create_service_account = true
}
