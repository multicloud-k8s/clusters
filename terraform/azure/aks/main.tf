#resource "azurerm_resource_group" "test" {
#  name     = "gothenburg-demo"
#  location = "North Europe"
#}

resource "azurerm_resource_group" "k8s-multicloud-demo" {
  name     = "k8s-multicloud-demo"
  location = "northeurope"
}

resource "azuread_application" "k8s-multicloud-demo" {
  name = "k8s-multicloud-demo"

  # wait 30s for server replication before attempting role assignment creation
  provisioner "local-exec" {
    command = "sleep 30"
  }
}

resource "random_string" "k8s-multicloud-demo" {
  length  = 33
  special = true
}

resource "azuread_application_password" "k8s-multicloud-demo" {
  application_id = azuread_application.k8s-multicloud-demo.id
  value          = random_string.k8s-multicloud-demo.result
  end_date       = "2020-01-01T01:02:03Z"

  # wait 30s for server replication before attempting role assignment creation
  provisioner "local-exec" {
    command = "sleep 30"
  }
}

resource "azuread_service_principal" "k8s-multicloud-demo" {
  application_id = azuread_application.k8s-multicloud-demo.application_id
}

module "aks" {
  source  = "Azure/aks/azurerm"
  CLIENT_SECRET = azuread_application_password.k8s-multicloud-demo.value
  CLIENT_ID = azuread_application.k8s-multicloud-demo.application_id
  prefix = "demo-k8s"
  kubernetes_version = "1.15.5"
  location = "northeurope"
}

output "id" {
  value = azuread_service_principal.k8s-multicloud-demo.id
}