terraform {
  backend "s3" {
    bucket = "k8s-multicloud-state"
    key    = "azure/bootstrap"
    region = "eu-west-1"
  }
}

