data "azurerm_subscription" "primary" {}

resource "azuread_application" "k8s-multicloud-demo" {
  name = "k8s-multicloud-demo"

  # wait 30s for server replication before attempting role assignment creation
  provisioner "local-exec" {
    command = "sleep 30"
  }
}

resource "random_string" "k8s-multicloud-demo" {
  length  = 33
  special = true
}

resource "azuread_application_password" "k8s-multicloud-demo" {
  application_id = azuread_application.k8s-multicloud-demo.id
  value          = random_string.k8s-multicloud-demo.result
  end_date       = "2020-01-01T01:02:03Z"

  # wait 30s for server replication before attempting role assignment creation
  provisioner "local-exec" {
    command = "sleep 30"
  }
}

resource "azuread_service_principal" "k8s-multicloud-demo" {
  application_id = azuread_application.k8s-multicloud-demo.application_id
}

resource "azurerm_role_assignment" "k8s-multicloud-demo" {
  scope              = data.azurerm_subscription.primary.id
  role_definition_id = "Contributor" # such as "Contributor"
  principal_id       = azuread_service_principal.k8s-multicloud-demo.id
}

output "secret" {
  value = azuread_application_password.k8s-multicloud-demo.value
}

output "id" {
  value = azuread_service_principal.k8s-multicloud-demo.id
}