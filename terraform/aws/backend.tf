terraform {
  backend "s3" {
    bucket = "k8s-multicloud-state"
    key    = "aws"
    region = "eu-west-1"
  }
}

